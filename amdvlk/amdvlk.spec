#To the extent possible under law, the person who associated CC0 with
#this spec file has waived all copyright and related or neighboring rights
#to this spec file.
#
#You should have received a copy of the CC0 legalcode along with this
#work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

Name: amdvlk
Version: 2020.03.6
Release: 0
Summary: AMD's open source vulkan driver
License: MIT
Source: %{name}-%{version}.tar.xz
URL: https://github.com/GPUOpen-Drivers/AMDVLK

BuildRequires: libopenssl-1_1-devel
BuildRequires: gcc-c++
BuildRequires: clang
BuildRequires: cmake
BuildRequires: git
BuildRequires: python-base
BuildRequires: glibc-devel
BuildRequires: libstdc++-devel
BuildRequires: libxcb-devel
BuildRequires: libX11-devel
BuildRequires: libxshmfence-devel
BuildRequires: libXrandr-devel
BuildRequires: wayland-devel
BuildRequires: lzop
BuildRequires: ninja
BuildRequires: llvm10
BuildRequires: llvm10-gold
%if 0%{?suse_version} == 1500
BuildRequires: dri2proto-devel
%endif

%description
The AMD Open Source Driver for Vulkan® is an open-source Vulkan driver for Radeon™ graphics adapters on Linux®. It is built on top of AMD's Platform Abstraction Library (PAL), a shared component that is designed to encapsulate certain hardware and OS-specific programming details for many of AMD's 3D and compute drivers. Leveraging PAL can help provide a consistent experience across platforms, including support for recently released GPUs and compatibility with AMD developer tools.

%prep
%setup -n drivers

%build
cd xgl
cmake -H. -Bbuilds/Release64 \
    -DCMAKE_AR=`which llvm-ar` \
    -DCMAKE_NM=`which llvm-nm` \
    -DCMAKE_RANLIB=`which llvm-ranlib` \
    -DCMAKE_BUILD_TYPE=Release \
    -DBUILD_WAYLAND_SUPPORT=On \
    -G Ninja
ninja -C builds/Release64

%install
mkdir -p %{buildroot}/%{_sysconfdir}/vulkan/icd.d/
mkdir -p %{buildroot}/%{_prefix}/lib64
cp xgl/builds/Release64/icd/amdvlk64.so $RPM_BUILD_ROOT/usr/lib64/
cp AMDVLK/json/Redhat/amd_icd64.json $RPM_BUILD_ROOT/etc/vulkan/icd.d/

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_prefix}/lib64
%{_prefix}/lib64/amdvlk64.so
%{_sysconfdir}/vulkan/
%{_sysconfdir}/vulkan/icd.d
%config %{_sysconfdir}/vulkan/icd.d/amd_icd64.json

%changelog
